import React from "react";

export type CellType = "primary" | "secondary";

type CellTypeToStyle = {
  [key in CellType]: string;
};

interface Props {
  style?: React.CSSProperties | undefined;
  className?: string | undefined;
  role?: string | undefined;
  children: React.ReactNode;
  cellType: CellType;
  colSpan: number;
  columnId: string;
  width?: string;
  customCellStyle?: string;
}

const tableCellSharedStyle = "whitespace-nowrap text-left";

export const tableCellStyle: CellTypeToStyle = {
  primary: "py-4 pl-4 text-sm text-gray-800",
  secondary: "py-1 pl-3 text-sm text-gray-800 font-medium",
};

const TableCell = ({
  children,
  cellType,
  colSpan,
  width,
  customCellStyle,
  columnId,
  ...rest
}: Props) => {
  const isNotDataCell = columnId === "selection" || columnId === "expander";

  return (
    <td
      {...rest}
      className={`${tableCellSharedStyle} ${
        tableCellStyle[cellType]
      } ${customCellStyle} ${isNotDataCell ? "px-2" : ""}`}
      colSpan={colSpan}
      style={{
        width: width ? width : isNotDataCell ? "4%" : "initial",
      }}
    >
      {children}
    </td>
  );
};

export default TableCell;
