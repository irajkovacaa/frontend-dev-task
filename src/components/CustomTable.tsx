import { useCallback, useEffect, useMemo } from "react";
import { useTable, usePagination, Column, useExpanded, Row } from "react-table";
import TableCheckbox from "./TableCheckbox";
import TablePagination from "./TablePagination";
import React from "react";
import TableCell, { CellType } from "./TableCell";
import TableHeader from "./TableHeader";
import TableRow from "./TableRow";

interface Props<T extends object> {
  columns: Column<T>[];
  totalResults?: number;
  data: T[];
  showHeader?: boolean;
  areCellsSelectable: boolean;
  showPagination?: boolean;
  subRowColSpan?: number;
  cellType: CellType;
  layoutAuto?: boolean;
  selectedRows?: string[];
  pageCount?: number;
  areAllCellsSelectable?: boolean;
  parentRowId?: string;
  customTableStyle?: string;
  setSelectedRows?: React.Dispatch<React.SetStateAction<string[]>>;
  fetchData?: (pageIndex: number, pageSize: number) => void;
  renderSubComponent?: (props: { row: Row<T> }) => React.ReactElement;
}

const wrapperClassName = "flex flex-col";
const tableContainerClassName = "w-full inline-block align-middle";
const tableWrapperClassName = "border border-primary shadow-md";
const tableClassName = "w-full divide-y divide-primary h-auto inline-block";
const tableBodyClassName =
  "bg-white divide-y divide-primary text-center w-full table";
const tableHeadClassName = "bg-gray2 sticky top-0 w-full table z-10";

export const CustomTable = <T extends object>({
  columns,
  totalResults,
  data,
  areCellsSelectable,
  showPagination,
  subRowColSpan,
  showHeader,
  cellType,
  layoutAuto,
  selectedRows,
  areAllCellsSelectable,
  parentRowId,
  pageCount: controlledPageCount,
  customTableStyle,
  setSelectedRows,
  fetchData,
  renderSubComponent,
}: Props<T>): JSX.Element => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 25 },
      pageCount: controlledPageCount,
      manualPagination: true,
      autoResetSelectedRows: false,
      getRowId: (row: any) => row.id,
      expandSubRows: false,
    },
    useExpanded,
    usePagination
  );

  useEffect(() => {
    if (fetchData) {
      fetchData(pageIndex, pageSize);
    }
  }, [fetchData, pageIndex, pageSize]);

  const getIdsWithSubrows = useCallback(
    <T extends object>(row: Row<T>): string[] => {
      const ids = [row.id];

      row.subRows.forEach((subrow) => {
        if (subrow.subRows.length > 0) {
          ids.push(...getIdsWithSubrows(subrow));
        } else {
          ids.push(subrow.id);
        }
      });

      return ids;
    },
    []
  );

  const onCellSelect = useCallback(
    <T extends object>(e: React.FormEvent<HTMLInputElement>, row: Row<T>) => {
      const idsWithSubrows = getIdsWithSubrows(row);

      if (setSelectedRows) {
        if (e.currentTarget.checked) {
          setSelectedRows((prev) => [...prev, ...idsWithSubrows]);
        } else {
          setSelectedRows((prev) =>
            prev.filter(
              (item) => !idsWithSubrows.includes(item) && item !== parentRowId
            )
          );
        }
      }
    },
    [getIdsWithSubrows, parentRowId, setSelectedRows]
  );

  const onAllCellSelect = useCallback(
    (e: React.FormEvent<HTMLInputElement>) => {
      const idsWithSubrows = page
        .map((row) => [...getIdsWithSubrows(row)])
        .flat();

      if (setSelectedRows) {
        if (e.currentTarget.checked) {
          setSelectedRows((prev) => [...prev, ...idsWithSubrows]);
        } else {
          setSelectedRows((prev) =>
            prev.filter((item) => !idsWithSubrows.includes(item))
          );
        }
      }
    },
    [getIdsWithSubrows, page, setSelectedRows]
  );

  const allRowsSelected = useMemo(() => {
    const idsWithSubrows = page
      .map((row) => [...getIdsWithSubrows(row)])
      .flat();

    const allSelected = idsWithSubrows.every((item) =>
      selectedRows?.includes(item)
    );

    return allSelected;
  }, [getIdsWithSubrows, page, selectedRows]);

  const onPageSizeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value;

    setPageSize(parseInt(value));

    if (setSelectedRows) {
      setSelectedRows([]);
    }
  };

  return (
    <div className={wrapperClassName}>
      <div>
        <div className={tableContainerClassName}>
          <div className={tableWrapperClassName}>
            <table
              {...getTableProps()}
              className={`${tableClassName} ${
                layoutAuto ? "table-auto" : "table-fixed"
              } ${customTableStyle} `}
            >
              <thead className={tableHeadClassName}>
                {showHeader
                  ? headerGroups.map((headerGroup, i) => {
                      const { key, ...rest } =
                        headerGroup.getHeaderGroupProps();
                      return (
                        <TableRow
                          {...rest}
                          key={`${key}-${i}`}
                          customRowStyle="bg-gray1"
                          disableHover
                        >
                          {areAllCellsSelectable ? (
                            <TableHeader colSpan={1}>
                              <TableCheckbox
                                onChange={(e) => {
                                  if (setSelectedRows) {
                                    onAllCellSelect(e);
                                  }
                                }}
                                checked={allRowsSelected}
                              />
                            </TableHeader>
                          ) : null}
                          {headerGroup.headers.map((column) => {
                            const { key, ...rest } = column.getHeaderProps();

                            return column.Header === "hide" ? null : (
                              <TableHeader
                                {...rest}
                                key={key}
                                colSpan={
                                  column.id !== "selection" &&
                                  column.id !== "expander"
                                    ? 2
                                    : 1
                                }
                                hasBorder={
                                  column.id !== "selection" &&
                                  column.id !== "expander"
                                }
                                width={column.headerWidth}
                              >
                                {column.render("Header")}
                              </TableHeader>
                            );
                          })}
                        </TableRow>
                      );
                    })
                  : null}
              </thead>
              <tbody {...getTableBodyProps()} className={tableBodyClassName}>
                {page.map((row) => {
                  prepareRow(row);

                  const { key, ...rest } = row.getRowProps();

                  return (
                    <React.Fragment key={`${key}-fragment`}>
                      <TableRow {...rest} key={key} cellType={cellType}>
                        {areCellsSelectable ? (
                          <TableCell
                            cellType="primary"
                            colSpan={1}
                            columnId="selection"
                          >
                            <TableCheckbox
                              onChange={(e) => {
                                onCellSelect(e, row);
                              }}
                              checked={selectedRows?.includes(row.id)}
                            />
                          </TableCell>
                        ) : null}

                        {row.cells.map((cell) => {
                          const { key, ...rest } = cell.getCellProps();

                          return cell.column.Cell !== "hide" ? (
                            <TableCell
                              key={key}
                              {...rest}
                              cellType={cellType}
                              colSpan={
                                cell.column.colSpan ? cell.column.colSpan : 1
                              }
                              columnId={cell.column.id}
                              width={cell.column.colWidth}
                            >
                              {cell.render("Cell")}
                            </TableCell>
                          ) : null;
                        })}
                      </TableRow>

                      {renderSubComponent && row.isExpanded ? (
                        <TableRow key={row.id + "-expanded"} disableHover>
                          <td colSpan={subRowColSpan || columns.length}>
                            {renderSubComponent({
                              row,
                            })}
                          </td>
                        </TableRow>
                      ) : null}
                    </React.Fragment>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      {showPagination ? (
        <TablePagination
          pageSize={pageSize}
          pageIndex={pageIndex}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          totalResults={totalResults || 0}
          nextPage={nextPage}
          previousPage={previousPage}
          onPageSizeChange={onPageSizeChange}
        />
      ) : null}
    </div>
  );
};

export default CustomTable;
