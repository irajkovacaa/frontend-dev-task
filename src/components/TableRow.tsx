import React from "react";
import { CellType } from "./TableCell";

interface Props {
  style?: React.CSSProperties | undefined;
  className?: string | undefined;
  role?: string | undefined;
  children: React.ReactNode;
  cellType?: CellType;
  customRowStyle?: string;
  disableHover?: boolean;
}

type CellTypeToRowStyle = {
  [key in CellType]: string;
};

export const tableRowStyle: CellTypeToRowStyle = {
  primary: "bg-white",
  secondary: "bg-gray3",
};

const TableRow = ({
  cellType,
  children,
  customRowStyle,
  disableHover,
  ...rest
}: Props) => {
  return (
    <tr
      {...rest}
      className={` ${cellType ? tableRowStyle[cellType] : ""} ${
        !disableHover ? "hover:bg-grayBlue" : ""
      } ${customRowStyle || ""}`}
    >
      {children}
    </tr>
  );
};

export default TableRow;
