import CustomSelect, { SelectOption } from "./CustomSelect";

interface Props {
  pageIndex: number;
  canPreviousPage: boolean;
  canNextPage: boolean;
  pageSize: number;
  totalResults: number;
  previousPage: () => void;
  nextPage: () => void;
  onPageSizeChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
}

const pageOptions: SelectOption[] = [
  {
    value: "25",
    label: "25",
  },
  {
    value: "50",
    label: "50",
  },
  {
    value: "100",
    label: "100",
  },
  {
    value: "150",
    label: "150",
  },
];

const wrapperClassName = "flex mt-4 pl-3 items-center justify-end mb-3";
const pageSizeSelectWrapper = "flex items-center";
const pageSizeLabelClassName = "shrink-0";
const showResultsTextClassName = "ml-3";
const buttonClassName =
  "font-bold cursor-pointer text-2xl px-1 disabled:opacity-50 disabled:cursor-default rounded-md ml-2  border border-primary";

const TablePagination = ({
  pageIndex,
  canNextPage,
  canPreviousPage,
  pageSize,
  totalResults,
  nextPage,
  previousPage,
  onPageSizeChange,
}: Props) => {
  const getShowingResultsString = () => {
    const start = pageIndex * pageSize + 1;

    const end = start - 1 + pageSize;

    return `${start} - ${
      end > totalResults ? totalResults : end
    } of ${totalResults}`;
  };

  return (
    <div className={wrapperClassName}>
      <div className={pageSizeSelectWrapper}>
        <div className={pageSizeLabelClassName}>Show rows:</div>
        <CustomSelect
          options={pageOptions}
          customOnChange={onPageSizeChange}
          customWrapperClasses="ml-3"
          value={pageSize.toString()}
        />
      </div>
      <div className={showResultsTextClassName}>
        {getShowingResultsString()}
      </div>
      <div>
        <button
          onClick={() => previousPage()}
          disabled={!canPreviousPage}
          className={buttonClassName}
        >
          {"<"}
        </button>
        <button
          onClick={() => nextPage()}
          disabled={!canNextPage}
          className={buttonClassName}
        >
          {">"}
        </button>
      </div>
    </div>
  );
};

export default TablePagination;
