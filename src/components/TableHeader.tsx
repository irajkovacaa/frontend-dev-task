import React from "react";

interface Props {
  style?: React.CSSProperties | undefined;
  className?: string | undefined;
  role?: string | undefined;
  colSpan: number;
  children: React.ReactNode;
  width?: string;
  hasBorder?: boolean;
}

const tableHeaderClassName = `px-3 py-3 text-xs font-bold uppercase text-darkerGrey text-left`;

const TableHeader = ({
  children,
  colSpan,
  width,
  hasBorder,
  ...rest
}: Props) => {
  return (
    <th
      {...rest}
      scope="col"
      className={`${tableHeaderClassName} ${
        hasBorder ? "border-r border-primary" : ""
      }`}
      style={{
        width: width || "initial",
      }}
      colSpan={colSpan}
    >
      {children}
    </th>
  );
};

export default TableHeader;
