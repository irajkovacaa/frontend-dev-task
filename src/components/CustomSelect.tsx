import React, { ClassAttributes, InputHTMLAttributes } from "react";

export interface SelectOption {
  value: string;
  label: string;
}

type SelectProps = {
  options: SelectOption[];
  maxWidthClass?: string;
  customWrapperClasses?: string;
  value: string;
  customOnChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
};

const selectClasses =
  "w-full bg-white py-1 px-4 rounded-md  border border-primary focus:outline-none cursor-pointer appearance-none mb-1";

const CustomSelect = ({
  options,
  customWrapperClasses,
  maxWidthClass,
  value,
  customOnChange,
  ...props
}: SelectProps &
  InputHTMLAttributes<HTMLSelectElement> &
  ClassAttributes<HTMLSelectElement>) => {
  const selectWrapperClasses = ` w-full ${
    maxWidthClass ? maxWidthClass : " max-w-md"
  }`;

  return (
    <div className={`${selectWrapperClasses} ${customWrapperClasses}`}>
      <select
        {...props}
        onChange={(e) => {
          customOnChange && customOnChange(e);
        }}
        className={`${selectClasses} `}
        value={value}
      >
        {options.map((option) => (
          <option value={option.value} key={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default CustomSelect;
