interface Props {
  label: string;
  color: string;
  showLabel?: boolean;
}

const wrapperClassName = "flex items-center";
const dotClassName = "w-2 h-2 rounded-full tooltip";
const labelClassName = "ml-2";

const StatusCell = ({ color, label, showLabel }: Props) => {
  return (
    <div className={wrapperClassName}>
      <div
        className={dotClassName}
        style={{
          backgroundColor: color,
        }}
      >
        <span className="tooltiptext">{label}</span>
      </div>
      {showLabel ? <div className={labelClassName}>{label}</div> : null}
    </div>
  );
};

export default StatusCell;
