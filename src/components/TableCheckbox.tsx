import { HTMLProps, useRef } from "react";

export default function TableCheckbox({
  onChange,
  checked,
}: {
  indeterminate?: boolean;
} & HTMLProps<HTMLInputElement>) {
  const ref = useRef<HTMLInputElement>(null);

  return (
    <input
      type="checkbox"
      ref={ref}
      className={" cursor-pointer"}
      onChange={onChange}
      checked={checked}
    />
  );
}
