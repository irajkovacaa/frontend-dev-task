import { CellProps } from "react-table";
import dropDownArrow from "../../../svg/arrow_drop_down_icon.svg";

const getCell = <T extends object>(
  cellData: CellProps<T>
): JSX.Element | null => {
  const { row } = cellData;

  return row.canExpand ? (
    <button {...row.getToggleRowExpandedProps()}>
      <img
        src={dropDownArrow}
        alt="dropdown-icon"
        loading="lazy"
        className="max-w-none w-5 h-5"
        style={{
          transition: "all .2s",
          transform: `rotate(${row.isExpanded ? "0deg" : "-90deg"})`,
          filter: row.isExpanded
            ? "invert(33%) sepia(93%) saturate(7464%) hue-rotate(206deg) brightness(107%) contrast(101%)"
            : "invert(69%) sepia(13%) saturate(101%) hue-rotate(185deg) brightness(92%) contrast(83%)",
        }}
      />
    </button>
  ) : null;
};

export const expanderColumn = {
  id: "expander",
  Cell: getCell,
  Header: "hide",
};
