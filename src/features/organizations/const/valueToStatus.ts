import { StatusType } from "../types/Status";

type ValueToStatus = {
  [key in StatusType]: {
    label: string;
    color: string;
  };
};

export const valueToStatus: ValueToStatus = {
  "0": {
    label: "N/A",
    color: "transparent",
  },
  "1": {
    color: "#4F4F4F",
    label: "Operational",
  },
  "2": {
    color: "#B63F3F",
    label: "Not operational",
  },
  "3": {
    color: "#F2C94C",
    label: "Partial",
  },
  "4": {
    color: "#F2C94C",
    label: "Pending",
  },
  "5": {
    color: "#BDBDBD",
    label: "Stopped",
  },
};
