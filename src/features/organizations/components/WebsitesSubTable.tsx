import { useContext, useMemo } from "react";
import { IOrganizationSubrows } from "../interfaces/IOrganizationSubrows";
import { CellProps, Column, Row } from "react-table";
import { IWebsitesTableData } from "../interfaces/IWebsitesTableData";
import CustomTable from "../../../components/CustomTable";
import StatusCell from "../../../components/StatusCell";
import { StatusType } from "../types/Status";
import { valueToStatus } from "../const/valueToStatus";
import { expanderColumn } from "../const/expanderColumn";
import SectionsSubComponent from "./SectionsSubComponent";
import { OrganizationsContext } from "../pages/Organizations";
import websiteIcon from "../../../svg/website-icon.svg";

interface Props {
  data: IOrganizationSubrows[];
  parentRowId: string;
}

const WebsitesSubTable = ({ data, parentRowId }: Props) => {
  const organizationsContext = useContext(OrganizationsContext);
  const columns: Column<IWebsitesTableData>[] = useMemo(
    () => [
      { ...expanderColumn },
      {
        accessor: "name",
        colWidth: "60%",
        Cell: (cellData: CellProps<IWebsitesTableData>) => {
          const { value } = cellData;

          return (
            <div className="flex items-center">
              <img
                src={websiteIcon}
                alt="website-icon"
                loading="lazy"
                className="max-w-none w-5 h-5"
              />
              <div className="ml-4">{value}</div>
            </div>
          );
        },
      },
      {
        accessor: "sections",
        colWidth: "18%",
      },
      {
        accessor: "status",
        Cell: (cellData: CellProps<IWebsitesTableData>) => {
          const { value } = cellData;

          const statusValue = value ? (value as StatusType) : 0;

          const { label, color } = valueToStatus[statusValue];

          return <StatusCell label={label} color={color} showLabel />;
        },
        colWidth: "18%",
      },
    ],
    []
  );

  const tableData = useMemo(() => {
    return {
      data: data.map((website) => ({
        id: website.id,
        name: website.name,
        status: website.status,
        sections: website.subRows.length,
        subRows: website.subRows.map((section) => ({
          id: section.id,
          name: section.name,
          status: section.status,
        })),
      })),
    };
  }, [data]);

  const renderSubComponent = (props: { row: Row<IWebsitesTableData> }) => {
    return (
      <SectionsSubComponent
        data={props.row.original.subRows}
        parentRowId={props.row.id}
      />
    );
  };

  return (
    <CustomTable
      columns={columns}
      data={tableData.data}
      cellType="primary"
      areCellsSelectable={true}
      subRowColSpan={5}
      selectedRows={organizationsContext?.selectedRows}
      parentRowId={parentRowId}
      setSelectedRows={organizationsContext?.setSelectedRows}
      renderSubComponent={renderSubComponent}
    />
  );
};

export default WebsitesSubTable;
