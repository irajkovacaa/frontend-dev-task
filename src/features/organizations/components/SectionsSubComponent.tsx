import { useContext, useMemo } from "react";
import { ISection } from "../interfaces/ISection";
import { CellProps, Column } from "react-table";
import { StatusType } from "../types/Status";
import { valueToStatus } from "../const/valueToStatus";
import StatusCell from "../../../components/StatusCell";
import CustomTable from "../../../components/CustomTable";
import { OrganizationsContext } from "../pages/Organizations";

interface Props {
  data: ISection[];
  parentRowId: string;
}

const containerClassName = "bg-gray4";

const tableContainerClassName = " flex flex-wrap justify-between";

const tableWrapperClassName = "my-6 mx-6 w-full max-w-xs";

const titleClassName =
  "text-xs text-gray7 font-bold text-left border-b border-primary py-1 pl-14";

const SectionsSubComponent = ({ data, parentRowId }: Props) => {
  const columns: Column<ISection>[] = useMemo(
    () => [
      {
        accessor: "name",
        colWidth: "70%",
      },
      {
        accessor: "status",
        Cell: (cellData: CellProps<ISection>) => {
          const { value } = cellData;

          const statusValue = value ? (value as StatusType) : 0;

          const { label, color } = valueToStatus[statusValue];

          return <StatusCell label={label} color={color} showLabel={false} />;
        },
        colWidth: "26%",
      },
    ],
    []
  );

  const organizationsContext = useContext(OrganizationsContext);

  const tablesData = useMemo(() => {
    const chunkSize = 4;

    const splitData = [];

    let idCounter = 0;

    for (let i = 0; i < data.length; i += chunkSize) {
      const chunk = data.slice(i, i + chunkSize);

      splitData.push({
        arrData: chunk,
        id: idCounter,
      });

      idCounter += 1;
    }

    return splitData;
  }, [data]);

  return (
    <div className={containerClassName}>
      <div className={titleClassName}>WEBSITE SECTIONS</div>

      <div className={tableContainerClassName}>
        {tablesData.map((tabData) => (
          <div key={`${tabData.id}`} className={tableWrapperClassName}>
            <CustomTable
              columns={columns}
              data={tabData.arrData}
              areCellsSelectable
              cellType="primary"
              layoutAuto
              selectedRows={organizationsContext?.selectedRows}
              parentRowId={parentRowId}
              setSelectedRows={organizationsContext?.setSelectedRows}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default SectionsSubComponent;
