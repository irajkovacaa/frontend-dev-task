import { createContext, useCallback, useMemo, useState } from "react";
import { IOrganizationsData } from "../interfaces/IOrganizationsData";
import { Column, Row } from "react-table";
import CustomTable from "../../../components/CustomTable";
import WebsitesSubTable from "../components/WebsitesSubTable";
import { IOrganizationsTableData } from "../interfaces/IOrganizationsTableData";
import { expanderColumn } from "../const/expanderColumn";

const API_URL = import.meta.env.VITE_API_URL;

const wrapperClassName = "max-w-1200 my-0 mx-auto";

export type OrganizationsContextType = {
  selectedRows: string[];
  setSelectedRows: React.Dispatch<React.SetStateAction<string[]>>;
};

export const OrganizationsContext =
  createContext<OrganizationsContextType | null>(null);

//add caching
const Organizations = () => {
  const [organizationsData, setOrganizationsData] =
    useState<IOrganizationsData>({
      result: [],
      "total-count": 0,
    });

  const [pageCount, setPageCount] = useState(0);

  const [selectedRows, setSelectedRows] = useState<string[]>([]);

  const getOrganizations = useCallback(
    async (pageIndex: number, pageSize: number) => {
      try {
        const response = await fetch(
          `${API_URL}/organizations/list?pageSize=${pageSize}&page=${
            pageIndex + 1
          }`
        );

        const data = await response.json();

        setOrganizationsData(data);

        setPageCount(
          Math.ceil((data as IOrganizationsData)["total-count"] / pageSize)
        );
      } catch (e) {
        setOrganizationsData({
          "total-count": 0,
          result: [],
        });

        setPageCount(0);
      }
    },
    []
  );

  const columns: Column<IOrganizationsTableData>[] = useMemo(
    () => [
      {
        ...expanderColumn,
      },
      {
        Header: "hide",
        accessor: "name",
        colSpan: 6,
      },
      {
        Header: "Website",
        Cell: "hide",
        headerWidth: "56%",
      },
      {
        Header: "Sections",
        Cell: "hide",
        headerWidth: "20%",
      },
      {
        Header: "Status",
        Cell: "hide",
        headerWidth: "20%",
      },
    ],
    []
  );

  const tableData = useMemo(() => {
    return {
      data: organizationsData.result.map((organization) => ({
        id: organization.id,
        name: organization.name,
        subRows: organization.websites.map((website) => ({
          id: website.id,
          name: website.name,
          status: website.status,
          sections: website.sections.length,
          subRows: website.sections.map((section) => ({
            id: section.id,
            name: section.name,
            status: section.status,
          })),
        })),
      })),
    };
  }, [organizationsData]);

  const renderSubComponent = (props: { row: Row<IOrganizationsTableData> }) => {
    return (
      <WebsitesSubTable
        data={props.row.original.subRows}
        parentRowId={props.row.id}
      />
    );
  };

  return (
    <OrganizationsContext.Provider
      value={{
        selectedRows,
        setSelectedRows,
      }}
    >
      <div className={wrapperClassName}>
        <CustomTable
          columns={columns}
          data={tableData.data}
          totalResults={organizationsData["total-count"]}
          cellType={"secondary"}
          customTableStyle="overflow-y-auto max-h-780"
          showPagination
          showHeader
          subRowColSpan={7}
          areCellsSelectable={false}
          areAllCellsSelectable={true}
          selectedRows={selectedRows}
          pageCount={pageCount}
          fetchData={getOrganizations}
          renderSubComponent={renderSubComponent}
          setSelectedRows={setSelectedRows}
        />
      </div>
    </OrganizationsContext.Provider>
  );
};

export default Organizations;
