import { IOrganizationSubrows } from "./IOrganizationSubrows";

export type IOrganizationsTableData = {
  id: number;
  name: string;
  subRows: IOrganizationSubrows[];
};
