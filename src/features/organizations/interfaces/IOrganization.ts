import { IWebsite } from "./IWebsite";

export interface IOrganization {
  id: number;
  name: string;
  websites: IWebsite[];
}
