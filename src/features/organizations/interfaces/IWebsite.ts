import { StatusType } from "../types/Status";
import { ISection } from "./ISection";

export interface IWebsite {
  id: number;
  name: string;
  status?: StatusType;
  sections: ISection[];
}
