import { StatusType } from "../types/Status";

export interface ISection {
  id: number;
  name: string;
  status: StatusType;
}
