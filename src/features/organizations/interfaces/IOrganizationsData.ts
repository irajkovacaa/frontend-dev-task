import { IOrganization } from "./IOrganization";

export interface IOrganizationsData {
  result: IOrganization[];
  "total-count": number;
}
