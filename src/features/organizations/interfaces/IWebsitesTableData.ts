import { StatusType } from "../types/Status";
import { ISection } from "./ISection";

export type IWebsitesTableData = {
  id: number;
  name: string;
  status?: StatusType;
  sections: number;
  subRows: ISection[];
};
