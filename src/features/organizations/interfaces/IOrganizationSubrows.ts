import { StatusType } from "../types/Status";
import { ISection } from "./ISection";

export interface IOrganizationSubrows {
  id: number;
  name: string;
  status?: StatusType;
  sections: number;
  subRows: ISection[];
}
