import Organizations from "./features/organizations/pages/Organizations";

const appClassName = "w-full min-h-screen bg-gray1 py-16";

function App() {
  return (
    <div className={appClassName}>
      <Organizations />
    </div>
  );
}

export default App;
