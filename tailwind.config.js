/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      gray1: "#333333",
      gray2: "#4F4F4F",
      gray3: "#828282",
      gray4: "#BDBDBD",
      gray5: "#E0E0E0",
      gray6: "#F2F2F2",
      gray7: " #9EA0A5",
      red: "#EB5757",
      black1: "#3E3F42",
      burgundy: "#B63F3F",
      orange: "#F2994A",
      yellow: "#F2C94C",
      green1: "#219653",
      green2: "#27AE60",
      green3: "#6FCF97",
      blue1: "#2F80ED",
      blue2: "#2D9CDB",
      blue3: "#56CCF2",
      purple1: "#9B51E0",
      purple2: "#BB6BD9",
    },
    backgroundColor: {
      gray1: "#F8F8FA",
      gray2: "#F7F7F8",
      gray3: "#F6F6F8",
      gray4: "#F7F8FA",
      white: "#FFFFFF",
      grayBlue: "#F6F9FD",
    },
    borderColor: {
      primary: "#EAEDF2",
    },
    divideColor: {
      primary: "#EAEDF2",
    },
    extend: {
      maxWidth: {
        1200: "1200px",
      },
      maxHeight: {
        780: "780px",
      },
    },
  },
  plugins: [],
};
